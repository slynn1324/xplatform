#!/bin/sh

# enable colors if we are writing to a terminal and it supports colors
if [ -t 1 ]; then 
	if which tput > /dev/null 2>&1 && [[ $(tput -T$TERM colors) -ge 8 ]]; then
	    GREEN='\033[0;32m'
		YELLOW='\033[1;33m'
		CYAN='\033[0;36m'
		NOCOLOR='\033[0m' # No Color
	fi
fi

echo ""
echo "${GREEN}building and testing xplatform sample${NOCOLOR}"
echo ""

if [ ! -d "build" ]; then
  mkdir "build"
fi

echo "${GREEN}checking if Emscripten SDK installed...${NOCOLOR}"
if [ ! -d "build/emsdk_portable" ]; then
	echo "${GREEN}Emscripten SDK not installed, installing...${NOCOLOR}"
	tar xvzf emsdk-portable.tar.gz -C build

	# Fetch the latest registry of available tools.
	build/emsdk_portable/emsdk update

	# Download and install the latest SDK tools.
	build/emsdk_portable/emsdk install latest

	# Make the "latest" SDK "active"
	build/emsdk_portable/emsdk activate latest
fi
echo "${GREEN}Emscripten SDK installed.${NOCOLOR}"


#
# Compile the things
#
echo ""
echo "${GREEN}Building things...${NOCOLOR}"
echo ""
echo ""

# compile c binary
echo "${YELLOW}Compiling C Binary${NOCOLOR}"
gcc functions.c -o build/functions
echo "${YELLOW}Compiled C Binary${NOCOLOR}"
echo ""

# compile JavaScript via Emscripten
echo "${YELLOW}Compiling JavaScript via Emscripten${NOCOLOR}"
python build/emsdk_portable/emscripten/1.35.0/emcc functions.c -s EXPORTED_FUNCTIONS="['_add','_getMessage']" -o build/functions.js
cp functions.html build/functions.html
echo "${YELLOW}Compiled JavaScript via Emscripten${NOCOLOR}"
echo ""

# compile JavaScript Node via Emscripten
echo "${YELLOW}Compiling Node JavaScript via Emscripten${NOCOLOR}"
python build/emsdk_portable/emscripten/1.35.0/emcc functions.c -o build/functions_node.js
echo "${YELLOW}Compiled Node JavaScript via Emscripten${NOCOLOR}"
echo ""


# compile Java
echo "${YELLOW}Compiling Java JNA${NOCOLOR}"
javac -cp jna.jar -d build FunctionsJNA.java
cp jna.jar build/jna.jar
# compile dynamiclib
echo "${YELLOW}Compiling Shared library for JNA${NOCOLOR}"
gcc -dynamiclib -o build/libfunctions.dylib functions.c
echo "${YELLOW}Compiled Java JNA${NOCOLOR}"
echo ""


# compile Java JS
echo "${YELLOW}Compile Java JS${NOCOLOR}"
javac -d build FunctionsJS.java
echo "${YELLOW}Compiled Java JS${NOCOLOR}"
echo ""

# copy Groovy JS
echo "${YELLOW}Copy Groovy JS${NOCOLOR}"
cp FunctionsJS.groovy build/FunctionsJS.groovy
echo "${YELLOW}Copied Groovy JS${NOCOLOR}"
echo ""


echo ""
echo ""
echo "${GREEN}Now let's test it...${NOCOLOR}"
echo ""
echo ""


#
# Test the things
# 

cd build

# c
echo "${CYAN}Testing C Binary:${NOCOLOR}"
./functions


# java
echo ""
echo "${CYAN}Testing Java JNA:${NOCOLOR}"
java -cp '.:jna.jar' FunctionsJNA

# java js
echo ""
echo "${CYAN}Testing Java JS:${NOCOLOR}"
java FunctionsJS

# groovy js
echo ""
echo "${CYAN}Testing FunctionsJS groovy:${NOCOLOR}"
groovy FunctionsJS.groovy

# javascript node
echo ""
echo "${CYAN}Testing JavaScript via Node:${NOCOLOR}"
emsdk_portable/node/4.1.1_64bit/bin/node functions_node.js


# javascript web
echo ""
echo "${CYAN}Testing JavaScript via browser:${NOCOLOR}"
open functions.html
echo "Opened file://$(pwd)/functions.html in Browser"

echo ""

echo ""
echo "${GREEN}all done.${NOCOLOR}"
echo ""





