# xplatform

Sample code of running simple C functions across many platforms.

# Requirements

(For these scripts, the tools are all cross-platform: Windows/Linux/MacOS)

- Mac OS X
- XCode Tools (LLVM)
- Java JDK (8+?)
- Groovy
- Python 2.7 (included in OS X)

# Running:

	git clone https://bitbucket.org/slynn1324/xplatform.git

	cd xplatform.git

    ./build.sh